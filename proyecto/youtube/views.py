from django.http import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from django.middleware.csrf import get_token

from .ytchannel import YTChannel
from . import data


def build_html(name, list, action, token):
    # Generamos el html, con todos los elementos del doc XML que necesito
    html = ""
    for video in list:
        html = html + data.VIDEO.format(link=video['link'],
                                   title=video['title'],
                                   id=video['id'],
                                   name=name,
                                   action=action,
                                   token=token,
                                   image=video['image'],
                                   published=video['published'],
                                   description=video['description'],
                                   url=video['url'])
    return html


def move_video(from_list, to_list, id):
    found = None
    for i, video in enumerate(from_list):
        if video['id'] == id:
            found = from_list.pop(i)
    if found:
        to_list.append(found)


def video(request, id):
    encontrado = None
    if request.method == 'GET':
        for i, video in enumerate(data.selected): # Buscamos el video con el id que le pasamos entre los videos seleccionados
            if video['id'] == id: # Si coinciden
                encontrado = video # Hemos encontrado el video
        if encontrado != None:
            htmlBody = data.INFORMACION.format(video=encontrado) # Cuando encontramos el video, mostramos toda su información
            return HttpResponse(htmlBody)
    htmlBody = data.ERROR.format(id=id) # En el caso, de que el id no se corresponda con el de ningún video seleccionado mostramos un página de error
    return HttpResponse(htmlBody)

def index(request):

    if request.method == 'POST':
        if 'id' in request.POST:
            if request.POST.get('select'):
                move_video(from_list=data.selectable,
                           to_list=data.selected,
                           id=request.POST['id'])
            elif request.POST.get('deselect'):
                move_video(from_list=data.selected,
                           to_list=data.selectable,
                           id=request.POST['id'])
    csrf_token = get_token(request)
    selected = build_html(name='deselect', list=data.selected,
                          action='Deselect', token=csrf_token)
    selectable = build_html(name='select', list=data.selectable,
                            action='Select', token=csrf_token)
    htmlBody = data.PAGE.format(selected=selected,
                                selectable=selectable)

    return HttpResponse(htmlBody)

