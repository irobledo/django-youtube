from django.apps import AppConfig
from . import data
from .ytchannel import YTChannel
import urllib.request

class YoutubeConfig(AppConfig):
    name = 'youtube' #Mismo nombre que la app
    def ready(self):
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg'
        xmlStream = urllib.request.urlopen(url) # Me proporciona el doc XML
        channel = YTChannel(xmlStream)
        data.selectable = channel.videos()


