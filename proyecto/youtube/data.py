selected = []
selectable = []

PAGE = """
    <!DOCTYPE html>
    <html lang="en">
        <body>
        <h1> Django Youtube </h1>
        <h5> Selected </h2>
            <ul>
            {selected}
            </ul>
        <h5> Selectable </h5>
            <ul>
            {selectable}
            </ul>
        </body>
    </html>
"""
VIDEO = """
      <li>
        <form action='/' method='POST'>
          <a href='{link}'>{title}</a>
          <input type='hidden' name='id' value='{id}'>
          <input type='hidden' name='csrfmiddlewaretoken' value='{token}'>
          <input type='hidden' name='{name}' value='True'> 
          <input type='submit' value='{action}'>
        </form>
      </li>
"""\

INFORMACION = """
      <!DOCTYPE html>
    <html lang="en">
        <body>
        <li>
        <h5>Nombre del canal: <a href='{video[url]}'>{video[name]}</a></h5>
        <h5>Título del video: <a href='{video[link]}'>{video[title]}</a></h5>
        <h5>Fecha: {video[published]}</h5>
        <h5>Descripción: {video[description]}</h5>
        <h5><a href='{video[link]}'><img src={video[image]}></a></h5>
        </li>
        </body>
    </html>
"""
ERROR = """
      <!DOCTYPE html>
    <html lang="en">
        <body>
       <h5>No existe video para el id: {id} </h5>
        </body>
    </html>
"""